const permit = (...roles) => {
    return (req, res, next) => {
        if(!req.user) res.status(401).send({message: "User unauthenticated!"});

        if(!roles.includes(req.user.role)) res.status(404).send({message: "Unauthorized"});

        next();
    }
};

module.exports = permit;