const express = require('express');
const Artist = require('../models/Artist');
const auth =require('../middlewares/auth');

const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});
const upload = multer({storage});

const createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {
        Artist.find().populate('Album')
            .then(artists => res.send(artists))
            .catch(() => res.sendStatus(500));
    });

    router.post('/', [auth, upload.single('photo')], (req, res) => {
        console.log(req.body);
        const artistData = req.body;
        if(req.file) {
            artistData.photo = req.file.filename;
        } else {
            artistData.photo = null;
        }

        const artist = new Artist(artistData);
        artist.save()
            .then(artist => res.send(artist))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;