const express = require('express');
const Track = require('../models/Track');
const auth = require('../middlewares/auth');

const createRouter = () => {
    const router = express.Router();
    let numberOfTracks = 0;
    Track.find().then(tracks => {
        numberOfTracks = tracks.length;
    });

    router.get('/', (req, res) => {
        if(req.query.album){
            Track.find({album: req.query.album}).populate('Album')
                .then(response => res.send(response))
                .catch(() => res.sendStatus(400));
        } else {
            Track.find().populate('Album')
                .then(tracks => {
                    res.send(tracks)
                })
                .catch(error => res.status(404).send(error));
        }
    });
    router.post('/',[auth], (req, res) => {
        const track = new Track(req.body);
        track.number = numberOfTracks + 1;
        numberOfTracks++;
        track.save()
            .then(track => res.send(track))
            .catch((error) => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;