const express = require('express');
const Album = require('../models/Album');
const auth = require('../middlewares/auth');

const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage})

const createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {
        Album.find().populate('Artist')
            .then(albums => res.send(albums))
            .catch(() => res.sendStatus(500));
    });

    router.get('/artistId/:artistId', (req, res) => {
        console.log(req.params);
        Album.find({artist: req.params.artistId})
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    })

    router.post('/', [auth, upload.single('coverImage')], (req, res) => {
        const albumData = req.body;
        if(req.file) {
            albumData.coverImage = req.file.filename;
        } else {
            albumData.coverImage = null;
        }

        const album = new Album(albumData);
        album.save()
            .then(album => res.send(album))
            .catch(error => res.status(400).send(error));
    });

    router.get('/:id', (req, res) => {
        const id = req.params.id;
        Album.findOne({_id: id}).populate('Artist')
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;