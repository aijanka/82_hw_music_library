const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema;
const nanoid = require('nanoid');

const UserSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: async function (value) {
                if(!this.isModified('username')) return true;

                const user = await User.findOne({username: value});
                if(user) throw new Error('User already exists');
            }
        },
        message: 'User already exists. Select another username!'
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        default: 'user',
        enum: ['user', 'admin']
    },
    token: String
});

const SALT_WORK_FACTOR = 10;

UserSchema.pre('save', async function (next) {
    console.log('first')
    if(!this.isModified('password')) next();
    const salt = await bcrypt.genSaltSync(SALT_WORK_FACTOR, (error, result) => {
        return result;
    });
    this.password = await bcrypt.hashSync(this.password, salt);
    console.log(this.password);


    next();
});

UserSchema.methods.checkPassword = function (password) {
    return bcrypt.compareSync(password, this.password, (error, response) => {
        console.log(response, 'check password');
    })
};

UserSchema.methods.generateToken = function() {
    this.token = nanoid();
};

UserSchema.set('toJSON', {
    transform: (doc, ret, options) => {
        delete ret.password;
        return ret;
    }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;