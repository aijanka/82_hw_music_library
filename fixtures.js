const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('albums');
        await db.dropCollection('artists');
        await db.dropCollection('tracks');
        await db.dropCollection('users');
    } catch (e) {
        console.log('There is no collections, skipping dropping...');
    }

    const [beyonce, mjackson, altj] = await Artist.create({
        name: 'Beyonce',
        photo: 'KWOb56t~BV7q7jbA2zSsL.jpeg',
        info: 'Some awesome singer and dancer'
        }, {
        name: 'Michael Jackson',
        photo: 'tFJhvJTrKqehXH8j3f48F.jpg',
        info: 'The most awesome singer and dancer'
        }, {
        name: 'Alt J',
        photo: '~2UavfU7iFbUYoSzSuKqC.jpeg',
        info: 'Some of the best alternative music groups'
        }
    );
    console.log('first');

    const [altjAlbum, mjacksonAlbum, beyonceAlbum] = await Album.create({
        artist: altj._id,
        releaseYear: 2014,
        title: "This is all yours",
        coverImage: "msqKZkuKnxsb~CGRKYJDT.jpeg"
    }, {
        artist: mjackson._id,
        releaseYear: 1987,
        title: 'Bad',
        coverImage: "220px-Michael_Jackson_-_Bad.jpg"
    }, {
        artist: beyonce._id,
        releaseYear: 2006,
        title: 'BDay',
        coverImage: ''
    });
    console.log('second');


    await Track.create({
        title: "Intro",
        album: altjAlbum._id,
        link:  "https://www.youtube.com/watch?v=UCbt_x5K7c8",
        number: 1
    }, {
        title: "Nara",
        album: altjAlbum._id,
        link: "https://www.youtube.com/watch?v=MtmrYisoxXA",
        number: 2
    }, {
        title: "Left hand Free",
        album: altjAlbum._id,
        link: "https://www.youtube.com/watch?v=NRWUoDpo2fo",
        number: 3
    }, {
        title: 'The way you make me feel',
        album: mjacksonAlbum._id,
        link: 'https://www.youtube.com/watch?v=HzZ_urpj4As',
        number: 4
    }, {
        title: 'Speed demon',
        album: mjacksonAlbum._id,
        link: 'https://www.youtube.com/watch?v=l039y9FaIjc',
        number: 5
    }, {
        title: 'Bad',
        album: mjacksonAlbum._id,
        link: 'https://www.youtube.com/watch?v=dsUXAEzaC3Q',
        number: 6
    }, {
        title: 'Liberian girl',
        album: mjacksonAlbum._id,
        link: 'https://www.youtube.com/watch?v=f3V-7DEAgdc',
        number: 7
    }, {
        title: 'Dirty Diana',
        album: mjacksonAlbum._id,
        link: 'https://www.youtube.com/watch?v=yUi_S6YWjZw',
        number: 8
    }, {
        title: 'Deja Vu',
        album: beyonceAlbum._id,
        link: 'https://www.youtube.com/watch?v=RQ9BWndKEgs',
        number: 9
    }, {
        title: 'Get me bodied',
        album: beyonceAlbum._id,
        link: 'https://www.youtube.com/watch?v=RioOJ7dZxuw',
        number: 10
    }, {
        title: 'Suga mama',
        album: beyonceAlbum._id,
        link: 'https://www.youtube.com/watch?v=nmP5CBiFigo',
        number: 11
    }, {
        title: 'Irreplaceable',
        album: beyonceAlbum._id,
        link: 'https://www.youtube.com/watch?v=2EwViQxSJJQ',
        number: 12
    }, {
        title: 'Check on it',
        album: beyonceAlbum._id,
        link: 'https://www.youtube.com/watch?v=Q1dUDzBdnmI',
        number: 13
    }, {
        title: 'The Gospel of John Hurt',
        album: altjAlbum._id,
        link: 'https://www.youtube.com/watch?v=LXkpWY5Y02Q',
        number: 14
    }, {
        title: 'Every other freckle',
        album: altjAlbum._id,
        link: 'https://www.youtube.com/watch?v=-mhgfXgwdls',
        number: 15
    });
    console.log('third');

    await User.create({
        username: 'admin',
        password: 'admin',
        role: 'admin'
    }, {
        username: 'user',
        password: ' user',
        role: 'user'
    })

    db.close();

});